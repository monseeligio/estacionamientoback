const express = require('express');
const {dbConnection} = require('./config/index');
const cors = require('cors');
const res = require('express/lib/response');
//const { use } = require('express/lib/application');
require('dotenv').config();
const app = express();

//Conexión a ruta

app.use(express.json());
app.use(cors());
dbConnection();
app.use(require('./Routes/routes'));

app.get('/', (res,req) => {

    res.json({
        ok:true,
        msj:"HOLA"
    });
});

////////////////////////////
app.listen(process.env.port,()=>{
    console.log("Bienvenido a mi primer back en el puerto: ",process.env.port);

});

