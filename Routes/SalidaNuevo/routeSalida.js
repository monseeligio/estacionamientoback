const express=require('express')

const modelSalidaNuevo= require('../../Models/estacionamiento/modelSalida')

let app=express();

app.post('/salida/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);
    let newSchemaSalida=new modelSalidaNuevo({
        placa:body.placa,
        cajon:body.cajon,
        FechaSalida:body.FechaSalida,
        horaSalida:body.horaSalida,
        tiempo:body.tiempo,
        total:body.total,
    });
    newSchemaSalida
    .save()
    .then(
        (data)=>{
            return res.status(200)
            .json({
                ok:true,
                mensaje:'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err)=>{
        return res.status(500)
        .json({
            ok:false,
            mensaje:'No se guardaron los datos',
            err
        });
    });
});


//consultar
app.get('/obtener/salida', async (req, res) => {
    const respuesta = await modelSalidaNuevo.find();
    res.status(200).json({
        ok: true,
        respuesta
    });
});

// eliminar 
app.delete('/delete/salida/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelSalidaNuevo.findByIdAndDelete(id);
    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

app.put('/update/renta/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;
    const respuesta = await modelSalidaNuevo.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});


module.exports=app;
