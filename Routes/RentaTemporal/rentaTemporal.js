const express=require('express')

const modelRentaTemporal= require('../../Models/estacionamiento/modeloRentaTemporal')

let app=express();

app.post('/renta/temporal', (req, res)=>{
    let body=req.body;
    console.log(body);
    let newSchemaTemporal=new modelRentaTemporal({
        placa:body.placa,
        nombreD:body.nombreD,
        cajon:body.cajon,
        periodo:body.periodo,
        cantidad:body.cantidad,
        fecha:body.fecha,
        totalT:body.totalT
    });
    newSchemaTemporal
    .save()
    .then(
        (data)=>{
            return res.status(200)
            .json({
                ok:true,
                mensaje:'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err)=>{
        return res.status(500)
        .json({
            ok:false,
            mensaje:'No se guardaron los datos',
            err
        });
    });
});


//consultar
app.get('/obtener/temporal', async (req, res) => {
    const respuesta = await modelRentaTemporal.find();
    res.status(200).json({
        ok: true,
        respuesta
    });
});

// eliminar 
app.delete('/delete/temporal/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelRentaTemporal.findByIdAndDelete(id);
    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

app.put('/update/temporal/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;
    delete campos.cajon
    delete campos.periodo
    delete campos.fecha
    const respuesta = await modelRentaTemporal.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});


module.exports=app;
