const express=require('express');
const app=express();
app.use(require('./test'));

app.use(require('./RentaNuevo/routeRenta'))
app.use(require('./SalidaNuevo/routeSalida'))
app.use(require('./registroUsuario/RouteUsuario'))
app.use(require('./RentaTemporal/rentaTemporal'))
app.use(require('./SalidaTemporal/routeSalidaTempora'))
module.exports=app;