const express = require('express');
const { Router } = require('express');
const model_users = require('../../Models/estacionamiento/newUsuario');
const md5 = require("bcryptjs");
const app = Router();
const Joi = require('@hapi/joi');
const jwt = require('jsonwebtoken');

const schemaRegister = Joi.object({
    nombreUsuario: Joi.string().min(6).max(255).required(),
    email: Joi.string().min(6).max(255).required().email(),
    password: Joi.string().min(6).max(1024).required(),
    role: Joi.string().min(6).max(255).required()
})


const schemaLogin = Joi.object({
    email: Joi.string().min(6).max(255).required().email(),
    password: Joi.string().min(6).max(1024).required(),
})

app.post('/login', async (req, res) => {

    const { error } = schemaLogin.validate(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})

    const user = await model_users.findOne({ email: req.body.email })
    if (!user) return res.json({ ok : false, error: true, mensaje: "email no registrado" })

    const passCorrecta = await md5.compare( req.body.password, user.password )
    if (!passCorrecta) return res.json({ ok : false, error: true, mensaje: "contraseña mal" })

    const token = jwt.sign({
        nombreUsuario: user.nombreUsuario,
        id: user._id,
        role: user.role
    }, process.env.token_secret)

    res.json({
        ok: true,
        error: null,
        mensaje: "bienvenido",
        role: user.role,
        token: token
    })
})

// registrar un usuario
app.post('/new/user', async (req, res) => {
    let body = req.body;

    //validacion
    const { error } = schemaRegister.validate(req.body)

    if (error) {
        return res.json(
            { error: error.details[0].message }
        )
    }

    const existeEmail = await model_users.findOne({ email: req.body.email })
    if (existeEmail) return res.json({ error: true, mensaje: "email ya registrado" })

    let registroUsuario = new model_users({

        nombreUsuario: body.nombreUsuario,
        email: body.email,
        password: md5.hashSync(body.password, 10),
        role: body.role
    });

    registroUsuario.save();

    res.json({
        ok: true,
        registroUsuario,
        msg: "Registro exitoso"
    });
});

// buscar usuario
app.get('/obtener/datos/usuarios', async (req, res) => {
    const respuesta = await model_users.find();


    res.status(200).json({
        ok: true,
        respuesta
    });
});

// buscar por id
app.get('/obtener/datos/usuarios/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await model_users.findById(id);


    res.status(200).json({
        ok: true,
        respuesta
    });
});


// eliminar usuario
app.delete('/delete/usuario/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await model_users.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

//actualizar usuario
app.put('/update/usuario/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await model_users.findByIdAndUpdate(id, campos, { new: true });

    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});

module.exports = app;