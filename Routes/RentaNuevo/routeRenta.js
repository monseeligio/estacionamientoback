const express=require('express')

const modelRentaNuevo= require('../../Models/estacionamiento/modelRenta')

let app=express();

app.post('/renta/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);
    let newSchemaRenta=new modelRentaNuevo({
        placa:body.placa,
        cajon:body.cajon,
        fecha:body.fecha,
        hora:body.hora,
    });
    newSchemaRenta
    .save()
    .then(
        (data)=>{
            return res.status(200)
            .json({
                ok:true,
                mensaje:'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err)=>{
        return res.status(500)
        .json({
            ok:false,
            mensaje:'No se guardaron los datos',
            err
        });
    });
});


//consultar
app.get('/obtener/renta', async (req, res) => {
    const respuesta = await modelRentaNuevo.find();
    res.status(200).json({
        ok: true,
        respuesta
    });
});

// eliminar 
app.delete('/delete/renta/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelRentaNuevo.findByIdAndDelete(id);
    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

app.put('/update/renta/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;
    delete campos.cajon
    delete campos.fecha
    delete campos.hora
    
    const respuesta = await modelRentaNuevo.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});


module.exports=app;
