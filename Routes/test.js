const express=require('express');
const { model } = require('mongoose');
const app=express();

app.get('/prueba', (req,res)=>{
    return res.json({
        ok:true,
        mensaje:'Prueba de server funcionando'
    });
});

module.exports=app;