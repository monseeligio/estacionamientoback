const express=require('express')

const modelSalidaTemporal= require('../../Models/estacionamiento/modeloSalidaRenta');

let app=express();

app.post('/salida/temporal', (req, res)=>{
    let body=req.body;
    console.log(body);
    let newSchemaSalidaTemporal=new modelSalidaTemporal({
        placa:body.placa,
        nombreD:body.nombreD,
        cajon:body.cajon,
        periodo:body.periodo,
        cantidad:body.cantidad,
        fecha:body.fecha,
        totalT:body.totalT
    });
    newSchemaSalidaTemporal
    .save()
    .then(
        (data)=>{
            return res.status(200)
            .json({
                ok:true,
                mensaje:'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err)=>{
        return res.status(500)
        .json({
            ok:false,
            mensaje:'No se guardaron los datos',
            err
        });
    });
});


//consultar
app.get('/obtenerSalida/temporal', async (req, res) => {
    const respuesta = await modelSalidaTemporal.find();
    res.status(200).json({
        ok: true,
        respuesta
    });
});

// eliminar 
app.delete('/deleteSalida/temporal/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelSalidaTemporal.findByIdAndDelete(id);
    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

app.put('/updateSalida/temporal/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;
    delete campos.cajon
    delete campos.periodo
    delete campos.fecha
    const respuesta = await modelSalidaTemporal.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});


module.exports=app;
