const mongoose=require('mongoose');
let Schema=mongoose.Schema;

let nuevoRentaTemporal = new Schema({
    placa:{type:String},
    nombreD:{type:String},
    cajon:{type:String},
    periodo:{type:String},
    cantidad:{type:Number},
    fecha:{type:Date},
    totalT:{type:Number}
});

module.exports=mongoose.model('NuevoRentaTemporal', nuevoRentaTemporal)