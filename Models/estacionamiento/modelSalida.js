const mongoose=require('mongoose');
let Schema=mongoose.Schema;

let nuevoSalidaEstacionamiento = new Schema({
    placa:{type:String},
    cajon:{type:String},
    FechaSalida:{type:Date},
    horaSalida:{type:String},
    tiempo:{type:String},
    total:{type:Number},
});

module.exports=mongoose.model('NuevoSalida', nuevoSalidaEstacionamiento)