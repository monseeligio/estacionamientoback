const mongoose=require('mongoose');
let Schema=mongoose.Schema;

let nuevoRentaEstacionamiento = new Schema({
    placa:{type:String},
    cajon:{type:Number},
    fecha:{type:Date},
    hora: {type:String},
});

module.exports=mongoose.model('NuevoRenta', nuevoRentaEstacionamiento)