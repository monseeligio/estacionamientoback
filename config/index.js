const mongoose = require('mongoose');
const dbConnection = async() => {
    try{
        await mongoose.connect(process.env.db_conexion,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
            console.log('DB ONLINE');
    } catch (error){
        console.log(error);
        throw new Error ("ERROR AL CONECTAR LA BASE DE DATOS")
    }
}

module.exports = {
    dbConnection
}